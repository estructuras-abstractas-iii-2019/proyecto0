#include "field.h"

using namespace std;

Field::Field(int cols, int rows){

	this->filas=rows;
	this->columnas=cols;
	this->playfield = new Cell*[this->filas];

	for (int i = 0; i < rows; ++i){
		playfield[i] = new Cell[this->columnas];
	}
}

Field::~Field(){}

void Field::mostrarTablero(){
	for (int i = -1; i < this->filas; ++i)
	{
		cout<<i+1<<" ";
		if (i==-1)
		{
			for (int j = 1; j < this->columnas+1; ++j)
			{
				cout<<" "<<j<<" ";
			}
		}
		if (i>-1)
		{
			for (int j = -1; j < this->columnas; ++j)
			{
				if (j>-1)
				{
					if ( this-> playfield[i][j].getArmy()==NULL) 
					 {
					 	cout<<" * ";
					 } else {
					 		cout << " " << this-> playfield[i][j].getArmy()->getType()<<" ";
					 }
				}
			}
		}
		cout<<endl<<endl;
	}
}

Cell** Field::getPlayfield(){return this->playfield;}