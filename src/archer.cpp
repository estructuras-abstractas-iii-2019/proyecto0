#include "archer.h"
#include "cell.h"

using namespace std;

const int Archer::costo=4;

Archer::Archer() : Unit{ 'A', "Arquero", 15, 9, 5, 3, 1}{}

Archer::~Archer(){}

void Archer::aumento_experiencia(){

	this->setXP(this->getXP()+1);

	if (this->getXP() >= (this->getLVL()*2))
	{
		this->levelUP();
		cout << "Arquero ha subido al nivel " << this->getLVL() << " ! " << endl << endl;
		this->estadoUnit();
	}
}

int Archer::getCosto(){return this->costo;}

bool Archer::moverse(Cell &casilla){//falta verificar que no este encerrada la unidad
	if (!casilla.esFranqueable()){
		return false;

	}

	return true;
}

bool Archer::atacar(Unit &enemigo){
enemigo.setSalud(enemigo.getSalud() - (this->getAtaque() - enemigo.getDefensa()) );
	if (enemigo.getSalud()<=0)
	{
		cout<<"El enemigo fue brutalmente atacado y ha muerto"<<endl<<endl;
		this->aumento_experiencia();

	} else {return false;}
return true;
}