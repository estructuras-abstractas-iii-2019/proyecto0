#include "player.h"
#include "archer.h"
#include "cavalry.h"
#include "lancer.h"
#include "cell.h"

using namespace std;

const int Player::maxCost=15;

Player::Player(string nombre, Field& campo):playfield(campo){
    this->nombre=nombre;
}

Player::~Player(){}

void Player::setscore(int score){
    this->score = score;
}

int Player::contarArmy(){
   int contador=0;
   while(this->army[contador]!=NULL){contador++;}
   return contador;
}



int Player::getscore(){    
    return this->score;
}

string Player::getnombre(){    
    return this->nombre;
}

const int Player::getmaxCost(){    
    return this->maxCost;
}

bool Player::createArmy(int Lancero, int Caballero, int Arquero){    
    
    if ( (Lancero*3+Caballero*5+Arquero*4) > this->maxCost)
    {
        return false;
    } else {
        int total= Arquero + Lancero + Caballero;

        for (int i=0; i < Arquero; ++i){
            this->army[i]= new Archer(); 
        }

        for (int i = Arquero; i < Caballero+Arquero; ++i){
            this->army[i]= new Cavalry();
        }
        
        for (int i = Caballero+Arquero; i < total; ++i){
            this->army[i]= new Lancer();
        }

        for (int i = total; i < 10; ++i){
            this->army[i]=NULL;
        }
    
        cout<<"Tiene "<<total<<" unidades en total: "<<endl;
        cout<<Arquero<<" arquero(s), ";
        cout<<Lancero<<" lancero(s) y ";
        cout<<Caballero<<" caballería(s)."<<endl<<endl;
        return true;
    }
}

void Player::colocarArmy1(){
    int contador=0;
    while(this->army[contador]!=NULL){ 
        this->playfield.getPlayfield()[contador][0].setArmy(this->army[contador]);
        this->playfield.getPlayfield()[contador][0].notFranqueable();
        this->army[contador]->setPosX(contador);
        this->army[contador]->setPosY(0);
        contador++;
    }
}

void Player::colocarArmy2(){
    int contador=0;
    while(this->army[contador]!=NULL){ 
        this->playfield.getPlayfield()[contador][4].setArmy(this->army[contador]);
        this->playfield.getPlayfield()[contador][4].notFranqueable();
        this->army[contador]->setPosX(contador);
        this->army[contador]->setPosY(4);
        contador++;
    }
}

void Player::play(){ 

    cout<<"----Es el turno de "<< this->nombre<<"----"<<endl<<endl;

    int contador=0;

    while(this->army[contador]!=NULL)
    {
        cout << "Desea mover la ficha " <<this->army[contador]->getNombre()<<"?"<<endl;

        char opcion;
        cin >> opcion;

        switch(opcion)
        {
            case 's':
            { 
                bool condicion=false;
                
                while(!condicion){
                    int coordX, coordY;
                    cout <<endl<< "A cuál casilla?"<<endl<<"     x: ";
                    cin>>coordX;
                    cout<<"     y: ";
                    cin>>coordY;
                    this->playfield.getPlayfield()[ coordX-1 ][ coordY-1 ].printEstadoCell();

                    condicion = this->army[contador]->moverse(this->playfield.getPlayfield()[coordX-1][coordY-1]);
                    
                    if(condicion){
                        this->playfield.getPlayfield()[ this->army[contador]->getPosX() ][ this->army[contador]->getPosY() ].notFranqueable();
                        this->playfield.getPlayfield()[ coordX-1 ][ coordY-1 ].notFranqueable();
                        this->playfield.getPlayfield()[ coordX-1 ][ coordY-1 ].setArmy(this->army[contador]);
                        this->playfield.getPlayfield()[ this->army[contador]->getPosX() ][ this->army[contador]->getPosY() ].setArmy(NULL);
                        
                    } else {
                        cout<<"No se puede mover a esa casilla"<<endl;
                    }

                    this->playfield.mostrarTablero();
                }
            }
            break;
            
            case 'n': 
                cout << "Ha seleccionado no mover la ficha"<<endl<<endl;
                this->playfield.mostrarTablero();
            break;
            

            default:
                cout << "Usted ha ingresado una opción incorrecta"<<endl<<endl;
        }


        cout << "Desea atacar con "<<this->army[contador]->getNombre()<<"?"<<endl;
        cin >> opcion;

        switch(opcion)
        {
            case 's':
            {
                bool condicion=false;

                while(!condicion){
                int coordX, coordY;
                cout << "A cuál enemigo desea atacar?"<<endl<<"       x: ";             
                cin>>coordX;
                cout << "       y: ";
                cin>>coordY;
                cout<<endl;

                condicion = this->army[contador]->atacar(*this->playfield.getPlayfield()[coordX-1][coordY-1].getArmy());
                    if (!condicion){
                        cout<<"No se ha podido realizar el ataque"<<endl;
                    }
                    this->score+=20;
                }
            }
            break;

            case 'n': cout << "Ha seleccionado no atacar";
            break;
            
            default: cout << "Usted ha ingresado una opción incorrecta";
        }
        ++contador;
        return;
    }
}