#include "UI.h"
#include <fstream>

using namespace std;
using std::ofstream;
using std::ios;


UI::UI(){
	cout<<endl<<"INICIO DEL JUEGO"<<endl<<endl<<"Presione <ENTER> para continuar "<<endl;
	getchar();
}

UI::~UI(){}

char UI::menu(){
	char opcion;
	cout<<"Seleccione una de las siguientes opciones: "<<endl<<endl<<"1 - - - > JUEGO NUEVO"<<endl<<"2 - - - > CARGAR PARTIDA"<<endl<<endl;
	cin>>opcion;
	cout<<endl<<endl;
	return opcion;
}

void UI::guardar(Unit &jugador){

ofstream archivoDatos("datos.dat", ios::out);


archivoDatos<<jugador.getXP()<<" "<<jugador.getNombre()<<" "<<jugador.getAtaque()<<" "
<<jugador.getType()<<" "<<jugador.getMaxSalud()<<" "<<jugador.getSalud()<<" "<<jugador.getDefensa()
<<" "<<jugador.getRango()<<" "<<jugador.getNivel()<<" "<<jugador.getMovimiento()
<<" "<<jugador.getPosX()<<" "<<jugador.getPosY()<<" "<<endl;

}
