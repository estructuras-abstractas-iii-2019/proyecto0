#include "unit.h"
#include "cell.h"

using namespace std;

Unit::Unit(){}

Unit::~Unit(){}

Unit::Unit(char tipo, string nombre, int maxSalud, int ataque, int defensa, int rango, int movimiento){
	
	this-> tipo = tipo;
	this-> nombre = nombre;
	this-> maxSalud = maxSalud;
	this-> salud = maxSalud;
	this-> ataque = ataque;
	this-> defensa = defensa;
	this-> rango = rango;
	this-> nivel = 1;
	this-> XP = 0;
	this-> movimiento = movimiento;
}

void Unit::setXP(int XP){this->XP=XP;}
int Unit::getXP(){return this->XP;}
int Unit::getLVL(){return this->nivel;}
string Unit::getNombre(){return this->nombre;}
int Unit::getAtaque(){return this->ataque;}
char Unit::getType(){return this->tipo;}

void Unit::levelUP(){
	this-> XP=0;
	this->nivel++;
	this->maxSalud += 0.25*maxSalud;
	this->salud += 0.25*salud;
	this->defensa += 0.25*defensa;
	this->ataque += 0.25*ataque;
}

void Unit::estadoUnit(){
	cout<<this->nombre<<" "<<this->id<<":"<<endl;
	cout<<"Nivel "<<this->nivel<<endl;
	cout<<"HP: "<<this->salud<<endl;
	cout<<"Ataque: "<<this->ataque<<endl;
	cout<<"Defensa: "<<this->defensa<<endl;
}

void Unit::setSalud(int HP){this->salud=HP;}
void Unit::setPosX(int posX){this->posX=posX;}
int Unit::getPosX(){return this->posX;}
int Unit::getPosY(){return this->posY;}			
void Unit::setPosY(int poxY){this->posY=posY;}
int Unit::getSalud(){return this->salud;}
int Unit::getDefensa(){return this->defensa;}
int Unit::getMovimiento(){return this->movimiento;}
int Unit::getMaxSalud(){return this->maxSalud;}
int Unit::getRango(){return this->rango;}
int Unit::getNivel(){return this->nivel;}