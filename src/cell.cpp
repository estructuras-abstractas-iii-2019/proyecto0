#include "cell.h"

using namespace std;

Cell::Cell(){}

Cell::~Cell(){}

void Cell::printEstadoCell(){
	
	if (this->franqueable){
		cout<<"Esta casilla está despejada"<<endl;	
	} else if (!this->franqueable) {
		cout<<"Esta casilla está ocupada por una unidad "<<this->army->getNombre()<<endl;
	}
}

bool Cell::esFranqueable(){return this->franqueable;}

Unit* Cell::getArmy(){return this->army;}

void Cell::setArmy(Unit *army){this->army=army;}

void Cell::notFranqueable(){this->franqueable=!franqueable;}