#include <iostream>
#include <string>
#include "cell.h"
#include "field.h"
#include "player.h"
#include "archer.h"
#include "cavalry.h"
#include "lancer.h"
#include "UI.h"

using namespace std;

int main(int argc, char const *argv[])
{
	UI interfaz;

	if (interfaz.menu()=='1'){
		Field *tablero = new Field(5,5);
		
		string nombres;
		cout<<"Digite el nombre del jugador 1:	";
		cin >> nombres;
		Player jugador1(nombres, *tablero);
		cout<<"Digite el nombre del jugador 2:	";
		cin>>nombres;
		Player jugador2(nombres, *tablero);
		cout<<endl;



		 cout<<jugador1.getnombre()<<", seleccione las unidades con las que desea jugar. Recuerde que el valor máximo es de 15, y que las unidades tienen los siguientes valores:"<<endl;
		 cout<<"Arquero: 4 "<<endl<<"Caballería: 5"<<endl<<"Lancero: 3"<<endl<<endl;
		 int caballeros, lanceros, arqueros;

		 cout<<"Cantidad de caballeros:	";
		 cin>>caballeros;
		 cout<<"Cantidad de arqueros:	";
		 cin>>arqueros;
		 cout<<"Cantidad de lanceros:	";
		 cin>>lanceros;
		 cout<<endl;


		 while(!jugador1.createArmy(lanceros,caballeros,arqueros)){

		 cout<<"Ha excedido el valor máximo de 15, intente de nuevo"<<endl;
		 cout<<jugador1.getnombre()<<", seleccione las unidades con las que desea jugar. Recuerde que el valor máximo es de 15, y que las unidades tienen los siguientes valores:"<<endl;
		 cout<<"Arquero: 4 "<<endl<<"Caballería: 5"<<endl<<"Lancero: 3"<<endl<<endl;
		
		 cout<<"Cantidad de caballeros:	";
		 cin>>caballeros;
		 cout<<"Cantidad de arqueros:	";
		 cin>>arqueros;
		 cout<<"Cantidad de lanceros:	";
		 cin>>lanceros;
		 cout<<endl;

		 }

		 jugador1.colocarArmy1();



		 cout<<jugador2.getnombre()<<", seleccione las unidades con las que desea jugar. Recuerde que el valor máximo es de 15, y que las unidades tienen los siguientes valores:"<<endl;
		 cout<<"Arquero: 4 "<<endl<<"Caballería: 5"<<endl<<"Lancero: 3"<<endl<<endl;
		

		 cout<<"Cantidad de caballeros:	";
		 cin>>caballeros;
		 cout<<"Cantidad de arqueros:	";
		 cin>>arqueros;
		 cout<<"Cantidad de lanceros:	";
		 cin>>lanceros;
		 cout<<endl;


		 while(!jugador2.createArmy(lanceros,caballeros,arqueros)){

		 cout<<"Ha excedido el valor máximo de 15, intente de nuevo"<<endl;
		 cout<<jugador2.getnombre()<<", seleccione las unidades con las que desea jugar. Recuerde que el valor máximo es de 15, y que las unidades tienen los siguientes valores:"<<endl;
		 cout<<"Arquero: 4 "<<endl<<"Caballería: 5"<<endl<<"Lancero: 3"<<endl<<endl;
		

		 cout<<"Cantidad de caballeros:	";
		 cin>>caballeros;
		 cout<<"Cantidad de arqueros:	";
		 cin>>arqueros;
		 cout<<"Cantidad de lanceros:	";
		 cin>>lanceros;
 		 cout<<endl;
		}

		jugador2.colocarArmy2();



		cout<<"LISTOS PARA JUGAR ! ! ! ! "<<endl<<endl;
		while(jugador1.contarArmy()!=0 && jugador2.contarArmy()!=0){
			cout<<"Desea guardar esta partida?: "<<endl<<endl;

			tablero->mostrarTablero();
			jugador1.play();
			cout<<jugador1.getnombre()<<" tiene "<<jugador1.getscore()<<" puntos ! "<<endl;

			cout<<"Desea guardar esta partida?: ";
			jugador2.play();
			cout<<jugador2.getnombre()<<" tiene "<<jugador2.getscore()<<" puntos ! "<<endl;

		}
		
	}
	return 0;
}