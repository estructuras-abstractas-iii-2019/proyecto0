#include "cavalry.h"
#include "cell.h"

using namespace std;

const int Cavalry::costo=5;

Cavalry::Cavalry() : Unit{ 'C', "Caballería", 25, 15, 5, 1, 3}{}

Cavalry::~Cavalry(){}

void Cavalry::aumento_experiencia(){

	this->setXP(this->getXP()+1);

	if (this->getXP() >= (this->getLVL()*2))
	{
		this->levelUP();
		cout << "Caballería ha subido al nivel " << this->getLVL() << " ! " << endl << endl;
		this->estadoUnit();
	}
}

int Cavalry::getCosto(){return this->costo;}

bool Cavalry::moverse(Cell &casilla){//falta verificar que no este encerrada la unidad
	if (!casilla.esFranqueable()){
		return false;

	} 
	return true;
}

bool Cavalry::atacar(Unit &enemigo){
	enemigo.setSalud(enemigo.getSalud() - (this->getAtaque() - enemigo.getDefensa()) );
	if (enemigo.getSalud()<=0)
	{
		cout<<"El enemigo fue brutalmente atacado y ha muerto"<<endl<<endl;
		this->aumento_experiencia();
	
	} else {return false;}
	return true;
}
