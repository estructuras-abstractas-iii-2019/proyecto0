#include "lancer.h"
#include "cell.h"

using namespace std;

const int Lancer::costo=3;

Lancer::Lancer() : Unit{ 'L', "Lancero", 20, 7, 7, 1, 1} {}

Lancer::~Lancer(){}

int Lancer::getCosto(){return this->costo;}

void Lancer::aumento_experiencia(){

	this->setXP(this->getXP()+1);

	if (this->getXP() >= (this->getLVL()*2))
	{
		this->levelUP();
		cout << "Lancero ha subido al nivel " << this->getLVL() << " ! " << endl << endl;
		this->estadoUnit();
	}
}


bool Lancer::moverse(Cell &casilla){//falta verificar que no este encerrada la unidad
	if (!casilla.esFranqueable()){return false;}
	return true;
}

bool Lancer::atacar(Unit &enemigo){
enemigo.setSalud(enemigo.getSalud() - (this->getAtaque() - enemigo.getDefensa()) );
	if (enemigo.getSalud()<=0)
	{
		cout<<"El enemigo fue brutalmente atacado y ha muerto"<<endl<<endl;
		this->aumento_experiencia();	
	} else {return false;}
return true;
}