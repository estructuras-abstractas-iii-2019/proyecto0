#pragma once

/**
 * @author Alexander Calderón Torres
 * @author Roberto Acevedo Mora
 * @date 10/02/2020
 * 
 * @file cell.h
 * @brief Definición de la clase Cell.
 */

#include "unit.h"

using namespace std;

/**
 * @class Cell
 * @brief Permite la creación de una casilla, que posteriormente en conjunto permiten conformar el campo de juego.
 */
class Cell{
public:

    /** 
     * @brief Constructor por defecto.
     */
     Cell();
    
    /** 
     * @brief Constructor por defecto.
     */
	~Cell();

	/** 
     * @brief Permite imprimir el estado actual de una casilla o celda
     */
	void printEstadoCell();

	/** 
     * @brief Retorna si la casilla es o no franqueable
     */
	bool esFranqueable();

	/** 
     * @brief Retorna un puntero a la unidad (Unit) posicionada en la casilla. Retorna NULL en caso de ser franqueable 
     */
	Unit* getArmy();

	/** 
     * @brief Método set para el puntero army de tipo Unit
     * @param *army puntero a una unidad
     */
	void setArmy(Unit *army);

	/** 
     * @brief Niega el valor actual de la variable franqueable
     */
	void notFranqueable();

private:
	bool franqueable=true;
	Unit* army;
};