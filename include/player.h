#pragma once


/**
 * @author Alexander Calderón Torres
 * @author Roberto Acevedo
 * @date 11/02/2020
 * @file player.h
 * @brief Aqui se define la estructura de la clase Player
 */


#include "field.h"

using namespace std;


/**
 *  @class Player
 *  @brief Clase Player que muestra los atributos privados y publicos
 */


class Player{

public:

    /**
    * @brief Constructor 
    * @param nombre parametro
    * @param &campo referencia de parametro constante
    */
    
    Player(string nombre, Field &campo);
    
    /**
    * @brief Destructor
    */
    
    ~Player();
    
    /**
      * @brief Funcion que introducir la cantidad de unidades
      * con la que cuenta cada ejercito
      */ 

    bool createArmy(int lancero, int caballeria, int arquero);
    
     /**
     * @brief Funcion que imprime el orden de las unidades para el jugador 1
     */
     
 	void colocarArmy1();
 	
 	/**
     * @brief Funcion que imprime el orden de las unidades para el jugador 2
     */
     
 	void colocarArmy2();
 	
 	/**
     * @brief Funcion que imprime los resultados
	 * @param score parametro entero
     */ 
     
	void setscore(int score);
	
	 /**
      * @brief Devuelve el puntaje del jugador
      */ 
      
	int getscore();
	
	  /**
      * @brief Devuelve el nombre del jugador
      */ 
      
	string getnombre();
	
	/**
    * @brief Devuelve el maximo costo permitido a cada jugador, es constante
    */ 
	const int getmaxCost();
	
	  /**
      * @brief Funcion que permite contar las unidades 
      */ 


	int contarArmy();
	
	 /**
      * @brief Funcion que lleva el control de la partida y las funciones mover
      * y atacar de cada jugador
      */ 
	
	
    void play();


private:

    string nombre;
    int score=0;
    static const int maxCost;
    Unit* army[10];
    Field& playfield;
};