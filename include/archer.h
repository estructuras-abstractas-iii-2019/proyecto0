#pragma once

/**
 * @author Alexander Calderón Torres
 * @author Roberto Acevedo Mora
 * @date 10/02/2020
 * 
 * @file archer.h
 * @brief Definición de la clase Archer. Permite la creación de este tipo de unidades posteriormente.
 */

#include "unit.h"

using namespace std;

/**
 * @class Archer
 * @brief Esta es una clase derivada de Unit, y permite instanciar a los arqueros en el juego.
 */
class Archer : public Unit{
public:
    /** 
     * @brief Constructor por defecto.
     */
	Archer();

    /** 
     * @brief Destructor por defecto.
     */
	~Archer();

    /** 
     * @brief Implementa el método virtual puro definido en la clase Unit. Permite a los arqueros moverse a distintas casillas
     * @param &casilla recibe una referencia a una casilla (Cell)
     */
	bool moverse(Cell &casilla);

   /** 
     * @brief Implementa el método virtual puro definido en la clase Unit. Permite a los arqueros atacar a los distintos enemigos
     * @param &enemigo recibe una referencia a una unidad enemiga (Unit)
     */
	bool atacar(Unit &enemigo);

	/** 
     * @brief Método get para la variable costo
     */
	int getCosto();
		
    /** 
     * @brief Método que aumenta la experiencia de la unidad
     */
	void aumento_experiencia();

private:
	static const int costo;
	static int idCount;

};