#pragma once


/**
 * @author Alexander Calderón Torres
 * @author Roberto Acevedo Mora
 * @date 10/02/2020
 * 
 * @file unit.h
 * @brief Definición de la clase abstracta Unit (no instanciable).
 */

#include <iostream>
#include <string>

using namespace std;
/**
 * @brief Forward declaration de Cell
 */
class Cell;

/**
 * @class Unit
 * @brief Esta es una clase abstracta que posee todos los atributos de las distintas unidades
 */
class Unit{

 public:
    /** 
     * @brief Constructor por defecto.
     */
	Unit();

    /** 
     * @brief Destructor por defecto.
     */	
	~Unit();
	
    /** 
     * @brief Constructor personalizado que define los atributos de una unidad.
     */
	Unit(char tipo, string nombre, int maxSalud, int ataque, int defensa, int rango, int movimiento);
	

    /** 
     * @brief Método virtual puro para el movimiento de cada unidad.
     * @param &casilla recibe una referencia a una casilla (Cell)
     */
	virtual bool moverse(Cell &casilla)=0;

	/** 
     * @brief Método virtual puro para el ataque de cada unidad.
     * @param &enemigo recibe una referencia a una unidad enemiga (Unit)
     */
	virtual bool atacar(Unit &enemigo)=0;
	
	/** 
     * @brief Método set para la variable XP
     */
	void setXP(int XP);

	/** 
     * @brief Método get para la variable XP
     */
	int getXP();

	/** 
     * @brief Método get para la variable LVL
     */
	int getLVL();

	/** 
     * @brief Método encargado de subir de nivel a la unidad
     */
	void levelUP();

	/** 
     * @brief Método que imprime los stats de la unidad
     */
	void estadoUnit();

	/** 
     * @brief Método set para la variable posX
     */
	void setPosX(int posX);		

	/** 
     * @brief Método get para la variable posX
     */
	int getPosX();	

	/** 
     * @brief Método set para la variable posY
     */
	void setPosY(int posY);

	/** 
     * @brief Método get para la variable posY
     */
	int getPosY();	

	/** 
     * @brief Método set para la variable salud
     */
	void setSalud(int HP);

	/** 
     * @brief Método get para la variable salud
     */
	int getSalud();

	/** 
     * @brief Método get para la variable nombre
     */
	string getNombre();

	/** 
     * @brief Método get para la variable defensa
     */
	int getDefensa();

	/** 
     * @brief Método get para la variable ataque
     */
	int getAtaque();

	/** 
     * @brief Método get para la variable tipo
     */
	char getType();
	
	/** 
     * @brief Método get para la variable count
     */
	
	int  getCount(); 
	
	/** 
     * @brief Método set para la variable count
     */
	
	void setCount(string accion);
	
	/** 
     * @brief Método get para la variable movimiento
     */
	
	int getMovimiento();
	
		/** 
     * @brief Método get para la variable salud
     */
	
	int getMaxSalud();
	
	/** 
     * @brief Método get para la variable rango
     */
	
	int getRango();
	
	/** 
     * @brief Método get para la variable nivel
     */
	
	int getNivel();
	
	
	
	
	
private:
	int id, maxSalud, salud, ataque, defensa, rango, nivel, XP, movimiento, posX, posY;
	string nombre;
	char tipo;
};