#pragma once

/**
 * @author Alexander Calderón Torres
 *@author Roberto Acevedo
 * @date 11/02/2020
 * @file UI.h
 * @brief Aqui se define la clase UI que permite llevar un control del juego, sus
 *partidas guardas y las que se cargan
 */


#include <iostream>
#include <string>
#include "player.h"
#include "unit.h"

using namespace std;

/**
 *  @class UI
 *  @brief Clase UI que muestra los atributos privados y publicos
 */

class UI
{
public:

   /**
    * @brief Constructor
    */

	UI();
	
	/**
    * @brief Destructor
    */
	~UI();
	
	/**
    * @brief Funcion que permite guardar los datos necesarios para cargar el juego
	* posteriormente si el usuario asi lo desea
    * @param parametro de referencia &jugador
    */


	void guardar(Unit &jugador);

	
	/**
    * @brief Funcion que permite desplegar un menu para elegir la opcion de
	* jugar o cargar partida
    */

	char menu();
	
	
};