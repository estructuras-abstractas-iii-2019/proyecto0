#pragma once

/**
 * @author Alexander Calderón Torres
 * @author Roberto Acevedo Mora
 * @date 10/02/2020
 * 
 * @file field.h
 * @brief Definición de la clase Field. Permite la creación del tablero de juego.
 */
#include "cell.h"

using namespace std;

/**
 * @class Field
 * @brief Clase que contiene el tablero de juego
 */
class Field
{
public:

	/** 
     * @brief Constructor personalizado.
     */
	Field(int cols, int rows);

	/** 
     * @brief Destructor por defecto.
     */
	~Field();

	/** 
     * @brief Método que retorna el campo de juego como un puntero doble
     */
	Cell** getPlayfield();

    /** 
     * @brief Muestra el tablero de juego
     */
	void mostrarTablero();
	
private:
	int filas, columnas;
	Cell **playfield;
};